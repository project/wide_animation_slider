<?php

namespace Drupal\wide_animation_slider\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\file\Entity\File;

/**
 * Configure Wide animation slider settings for this site.
 */
class WideSliderForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wide_slider_form_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'wide_animation_slider.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('wide_animation_slider.settings');
    $form['wide_slider'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Wide Animation Slider'),
      '#open' => TRUE,
    ];
    $no_of_slides = 5;
    for ($i = 1; $i <= $no_of_slides; $i++) {
      $form['wide_slider']['slide' . $i] = [
        '#type' => 'details',
        '#title' => $this->t('Slide @i.', ['@i' => $i]),
        '#collapsible' => true,
        '#collapsed' => true,
      ];

      $form['wide_slider']['slide' . $i]['slide' . $i . '_image'] = [
        '#type' => 'managed_file',
        '#upload_location'  => 'public://bk-imgs',
        '#title' => t('Upload Slider Image'),
        '#default_value' => $config->get("slide{$i}_image"),
        '#attributes' => [
          '#multiple' => TRUE,
        ],
        '#upload_validators' => [
          'file_validate_extensions' => ['gif png jpg jpeg webp'],
        ],
      ];

      $form['wide_slider']['slide' . $i]['slide' . $i . '_title'] = [
        '#type' => 'textfield',
        '#title' => t('Write Wide Slider Title'),
        '#default_value' => $config->get("slide{$i}_title"),
      ];

      $form['wide_slider']['slide' . $i]['slide' . $i . '_url'] = [
        '#type' => 'textfield',
        '#title' => t('Write Wide Slider Link'),
        '#default_value' => $config->get("slide{$i}_url"),
      ];
    }
  
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('wide_animation_slider.settings');
    $no_of_slides = 5;
    for ($i = 1; $i <= $no_of_slides; $i++) {
      $config->set("slide{$i}_image", $form_state->getValue("slide{$i}_image"));
      $config->set("slide{$i}_title", $form_state->getValue("slide{$i}_title"));
      $config->set("slide{$i}_url", $form_state->getValue("slide{$i}_url"));
      $image_fid = $config->get("slide{$i}_image");
      if (!empty($image_fid)) {
        $file = \Drupal::entityTypeManager()->getStorage('file')->load($image_fid[0]);
        $file->setPermanent();
        $file->save();
      }
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
