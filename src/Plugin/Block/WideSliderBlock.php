<?php

/**
 * @file
 * Contains \Drupal\wide_animation_slider\Plugin\Block\SliderBlock.
 */

namespace Drupal\wide_animation_slider\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\file\Entity\File;

/**
 * Provides a 'wide slider block' block.
 *
 * @Block(
 *   id = "wide_slider_block",
 *   admin_label = @Translation("Wide Animation Slider Block"),
 *   category = @Translation("Wide Animation Slider Block")
 * )
 */
class WideSliderBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    global $base_url;
    $module_path = $base_url . '/' . \Drupal::service('extension.list.module')->getPath('wide_animation_slider');
    $form = \Drupal::formBuilder()->getForm('Drupal\wide_animation_slider\Form\WideSliderForm');

    $wide_content = [];
    $config = \Drupal::config('wide_animation_slider.settings');
    $no_of_slides = 5;

    for ($i = 1; $i <= $no_of_slides; $i++) {
      $wide_fid = $config->get("slide{$i}_image");
      $wide_title = $config->get("slide{$i}_title");
      $wide_link = $config->get("slide{$i}_url");
      $wide_url = "";
      if (!empty($wide_fid)) {
				$wide_obj = File::load($wide_fid[0]);
				$uri = $wide_obj->getFileUri();
				$wide_url = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
			}

      $wide_content[$i]['wide_fid'] = $config->get("slide{$i}_image");
      $wide_content[$i]['wide_title'] = $config->get("slide{$i}_title");
      $wide_content[$i]['wide_link'] = $config->get("slide{$i}_url");
      $wide_content[$i]['wide_image'] = $wide_url;
    }
    return [
			'#theme' => 'block__wide_slider_block',
			'#form' => $form,
			'wide_content' => $wide_content,
			'#module_path' => $module_path,
			'#attached' => [
				'library' => [
					'wide_animation_slider/global_style',
				],
      ],
    ];
  }
}
